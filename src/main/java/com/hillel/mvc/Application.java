package com.hillel.mvc;

import com.hillel.mvc.connector.console.ConsoleConnector;

public class Application
{
    public static void main(String[] args)
    {
        ConsoleConnector.init();

        ConsoleConnector.start("item");
    }
}
