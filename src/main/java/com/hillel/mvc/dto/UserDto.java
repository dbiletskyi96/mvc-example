package com.hillel.mvc.dto;

import java.util.Objects;

public final class UserDto
{
    private String login;
    private String password;

    public UserDto()
    {
    }

    public UserDto(String login, String password)
    {
        this.login = login;
        this.password = password;
    }

    public String getLogin()
    {
        return login;
    }

    public UserDto setLogin(String login)
    {
        this.login = login;
        return this;
    }

    public String getPassword()
    {
        return password;
    }

    public UserDto setPassword(String password)
    {
        this.password = password;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        UserDto userDto = (UserDto) o;
        return Objects.equals(login, userDto.login) &&
               Objects.equals(password, userDto.password);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(login, password);
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("UserDto{");
        sb.append("login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
