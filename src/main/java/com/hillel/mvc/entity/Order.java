package com.hillel.mvc.entity;

public class Order extends Entity<String>
{

    public Integer userId;

    public Integer getUserId()
    {

        return userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }
}
