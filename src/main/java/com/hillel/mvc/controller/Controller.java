package com.hillel.mvc.controller;

public interface Controller<T, ID>
{
    T post(T entity); //create
    T get(ID id); //get
    T put(T entity); //update
    void delete(T entity); //delete
}
