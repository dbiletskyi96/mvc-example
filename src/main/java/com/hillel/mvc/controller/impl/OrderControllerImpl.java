package com.hillel.mvc.controller.impl;

import com.hillel.mvc.controller.Controller;
import com.hillel.mvc.dto.OrderDto;

import java.util.UUID;

public class OrderControllerImpl implements Controller<OrderDto, UUID>
{
    @Override
    public OrderDto post(OrderDto entity)
    {
        return null;
    }

    @Override
    public OrderDto get(UUID uuid)
    {
        return null;
    }

    @Override
    public OrderDto put(OrderDto entity)
    {
        return null;
    }

    @Override
    public void delete(OrderDto entity)
    {

    }
}
