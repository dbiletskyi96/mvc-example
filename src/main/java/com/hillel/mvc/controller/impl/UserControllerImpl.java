package com.hillel.mvc.controller.impl;

import com.hillel.mvc.controller.Controller;
import com.hillel.mvc.dto.UserDto;
import com.hillel.mvc.dto.UserOrderDto;
import com.hillel.mvc.entity.Order;
import com.hillel.mvc.entity.User;
import com.hillel.mvc.mapper.impl.UserOrderMapperImpl;
import com.hillel.mvc.mapper.impl.UserMapperImpl;
import com.hillel.mvc.service.Service;
import com.hillel.mvc.service.impl.UserServiceImpl;

import java.util.UUID;

public class UserControllerImpl
    implements Controller<UserDto, Integer>
{
    private Service<User, Integer> userService;
    private Service<Order, UUID>   orderService;


    public UserControllerImpl()
    {
        userService = new UserServiceImpl();
    }

    public UserControllerImpl(Service<User, Integer> userService)
    {
        this.userService = userService;
    }

    @Override
    public UserDto post(UserDto entity)
    {
        User user = new UserMapperImpl().mapToObject(entity);

        User createdUser = userService.create(user);
        Order order = orderService.get(UUID.randomUUID());

        return new UserMapperImpl().mapToDto(user);
    }

    @Override
    public UserDto get(Integer integer)
    {
        return null;
    }

    @Override
    public UserDto put(UserDto entity)
    {
        return null;
    }

    @Override
    public void delete(UserDto entity)
    {

    }


    public UserOrderDto post(UserDto entity, String query)
    {
        User user = new UserMapperImpl().mapToObject(entity);

        User createdUser = userService.create(user);
        Order order = orderService.get(UUID.randomUUID());

        return new UserOrderMapperImpl().mapToObject(null);
    }

}
