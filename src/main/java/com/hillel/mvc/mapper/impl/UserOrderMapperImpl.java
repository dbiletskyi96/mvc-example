package com.hillel.mvc.mapper.impl;

import com.hillel.mvc.dto.UserOrderDto;
import com.hillel.mvc.entity.Order;
import com.hillel.mvc.entity.User;
import com.hillel.mvc.mapper.Mapper;
import javafx.util.Pair;

public class UserOrderMapperImpl implements Mapper<Pair<User,Order>, UserOrderDto>
{
    @Override
    public UserOrderDto mapToObject(Pair<User, Order> origin)
    {
        return null;
    }

    @Override
    public Pair<User, Order> mapToDto(UserOrderDto object)
    {
        return null;
    }
}
