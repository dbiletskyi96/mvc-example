package com.hillel.mvc.mapper.impl;

import com.hillel.mvc.dto.UserDto;
import com.hillel.mvc.entity.User;
import com.hillel.mvc.mapper.Mapper;

public class UserMapperImpl implements Mapper<UserDto, User>
{
    @Override
    public User mapToObject(UserDto userDto)
    {
        User user = new User();
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        return user;
    }

    @Override
    public UserDto mapToDto(User object)
    {
        return new UserDto(object.getLogin(), object.getPassword());
    }
}
