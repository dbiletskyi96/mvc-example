package com.hillel.mvc.dao;

public interface EntityDao<T, ID>
{
    T create(T entity) throws Exception; //create
    T find(ID id); //get
    T update(T entity); //update
    int delete(T entity); //delete
}
