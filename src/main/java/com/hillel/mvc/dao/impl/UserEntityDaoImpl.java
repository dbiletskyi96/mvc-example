package com.hillel.mvc.dao.impl;

import com.hillel.mvc.dao.EntityDao;
import com.hillel.mvc.entity.User;

public class UserEntityDaoImpl implements EntityDao<User, Integer>
{
    @Override
    public User create(User entity)
    {
        return null;
    }

    @Override
    public User find(Integer integer)
    {
        return null;
    }

    @Override
    public User update(User entity)
    {
        return null;
    }

    @Override
    public int delete(User entity)
    {
        return 0;
    }
}
