package com.hillel.mvc.dao.impl;

import com.hillel.mvc.dao.EntityDao;
import com.hillel.mvc.entity.Order;

import java.util.UUID;

public class NewOrderDao implements EntityDao<Order, UUID>
{
    @Override
    public Order create(Order entity)
        throws Exception
    {
        return null;
    }

    @Override
    public Order find(UUID uuid)
    {
        return null;
    }

    @Override
    public Order update(Order entity)
    {
        return null;
    }

    @Override
    public int delete(Order entity)
    {
        return 0;
    }
}
