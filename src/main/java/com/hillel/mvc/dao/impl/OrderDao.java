package com.hillel.mvc.dao.impl;

import com.hillel.mvc.common.Constants;
import com.hillel.mvc.connector.db.DBFactory;
import com.hillel.mvc.dao.EntityDao;
import com.hillel.mvc.entity.Order;
import com.hillel.mvc.exceptions.MyDBException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class OrderDao implements EntityDao<Order, UUID>
{
    @Override
    public Order create(Order entity)
    {
        try(Connection connection = DBFactory.getMySQLInstance().getConnection())
        {
            PreparedStatement statement = connection.prepareStatement(Constants.CONSTANTA);
            connection.setTransactionIsolation(4);
            connection.setAutoCommit(true);

            statement.setInt(1, entity.getUserId());

            ResultSet resultSet = statement.executeQuery();

            Order order = new Order();
            while (resultSet.next()){
                //setItems
            }

            return order;
        }
        catch (SQLException e)
        {
            //че тут писать?
            throw new MyDBException(e);
        }
    }

    @Override
    public Order find(UUID uuid)
    {
        return null;
    }

    @Override
    public Order update(Order entity)
    {
        return null;
    }

    @Override
    public int delete(Order entity)
    {
        return 0;
    }
}
