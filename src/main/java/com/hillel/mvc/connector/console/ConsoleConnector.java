package com.hillel.mvc.connector.console;

import com.hillel.mvc.controller.Controller;
import com.hillel.mvc.controller.impl.ItemControllerImpl;
import com.hillel.mvc.controller.impl.OrderControllerImpl;
import com.hillel.mvc.controller.impl.UserControllerImpl;
import com.hillel.mvc.dto.UserDto;

import java.util.HashMap;
import java.util.Map;

public class ConsoleConnector
{
    private static Map<String, Controller> commandMap;
    private static Map<String, Object> session;

    public static void init()
    {
        commandMap = new HashMap<>();

        commandMap.put("user", new UserControllerImpl());
        commandMap.put("order", new OrderControllerImpl());
        commandMap.put("item", new ItemControllerImpl());

    }

    public static void start()
    {
        while (true)
        {

        }
    }

    public static void start(String command)
    {
        //get from console
        Controller controller = commandMap.get(command);

        System.out.println(controller.getClass());

        String who = "user";

        //UserConsoleHandler -

        String whatIshouldDo = "post";

        //get object

        UserDto dto = new UserDto();

        //get fields

        dto.setLogin("login");

        //get

        dto.setPassword("pass");

        switch (whatIshouldDo)
        {
            case "create":
                ((UserControllerImpl)controller).post(dto, "query");
                break;
            case "get":
                controller.get(1);
                break;
            case "update":
                controller.put(dto);
                break;
            case "delete":
                controller.delete(dto);
                break;
        }
    }

}
