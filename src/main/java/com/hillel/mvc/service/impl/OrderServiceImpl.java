package com.hillel.mvc.service.impl;

import com.hillel.mvc.common.Constants;
import com.hillel.mvc.dao.EntityDao;
import com.hillel.mvc.dao.impl.NewOrderDao;
import com.hillel.mvc.dao.impl.OrderDao;
import com.hillel.mvc.entity.Order;
import com.hillel.mvc.exceptions.MyApplicationException;
import com.hillel.mvc.service.Service;

import java.util.UUID;

public class OrderServiceImpl implements Service<Order, UUID>
{
    private EntityDao<Order, UUID> orderDao;

    public OrderServiceImpl()
    {
        orderDao = new OrderDao();
    }

    @Override
    public Order create(Order entity)
    {
        //validation
        //check if exist
        //check if item is present

        String a = Constants.CONSTANTA;

        try
        {
            return orderDao.create(entity);
        }
        catch (Exception e)
        {
            throw new MyApplicationException(e);
        }
    }

    @Override
    public Order get(UUID uuid)
    {
        return null;
    }

    @Override
    public Order update(Order entity)
    {
        return null;
    }

    @Override
    public int delete(Order entity)
    {
        return 0;
    }
}
