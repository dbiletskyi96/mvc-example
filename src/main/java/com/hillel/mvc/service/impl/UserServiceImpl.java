package com.hillel.mvc.service.impl;

import com.hillel.mvc.entity.User;
import com.hillel.mvc.service.Service;

import java.util.Collections;
import java.util.List;

public class UserServiceImpl implements Service<User, Integer>
{

    @Override
    public User create(User entity)
    {
        return null;
    }

    @Override
    public User get(Integer integer)
    {
        return null;
    }

    @Override
    public User update(User entity)
    {
        return null;
    }

    @Override
    public int delete(User entity)
    {
        return 0;
    }

    public List<User> getAllUsersWithOrders(){
        return Collections.emptyList();
    }
}
