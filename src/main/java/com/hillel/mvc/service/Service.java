package com.hillel.mvc.service;

public interface Service<T, ID>
{
    T create(T entity); //create
    T get(ID id); //get
    T update(T entity); //update
    int delete(T entity); //delete
}
